module javafx {
    requires javafx.controls;
    requires javafx.fxml;
	requires javafx.graphics;
	requires java.sql;
    requires mongo.java.driver;

    opens javafx to javafx.fxml;
    opens javafx.controller to javafx.fxml;
    exports javafx;
    exports javafx.controller;
    exports clases;
}