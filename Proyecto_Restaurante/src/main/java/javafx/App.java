package javafx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.PropertyFile;
import util.Util;

import java.io.IOException;

public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(Util.getFxmlLoader("layoutPortada").load());
        stage.setScene(scene);
        stage.show();
    }

    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(Util.getFxmlLoader(fxml).load());
    }

    public static void main(String[] args) throws IOException {
        PropertyFile.inicializar();
        launch();

    }
}
