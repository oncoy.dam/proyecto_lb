package javafx.controller;

import clases.Cliente;
import gestor_persistencia.GestorPersistencia;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import util.Util;

/**
 * The type Controller editar cliente.
 *
 * @author Liling Xu
 */
public class ControllerEditarCliente {
    public Label mensajeError;
    public Button botonGuardar;
    public Label dniLB;
    public TextField nombreTF;
    public TextField telefonoTF;
    public TextField calleTF;
    public Cliente cliente;
    private GestorPersistencia gestor;

    /**
     * Guarda el cliente con los cambios realizados, si los campos són válidos.
     *
     * @param actionEvent the action event
     * @author Liling Xu
     */
    public void guardarCambiosCliente(ActionEvent actionEvent) {
        if (comprobarCampos()) {
            cliente.setNombre(nombreTF.getText());

            cliente.setTelefono(Integer.parseInt(telefonoTF.getText()));

            cliente.setCalle(calleTF.getText());

            gestor.actualizarCliente(cliente);

            Util.cerrarVentana(actionEvent);
        }
    }

    /**
     * Comprueba que el teléfono sea válido.
     *
     * @return the boolean
     * @author Liling Xu
     */
    public boolean comprobarCampos() {
        mensajeError.setVisible(false);
        if (Util.comprobarTelefono(telefonoTF.getText())) {
            return true;
        } else {
            mensajeError.setText("El nuevo número del teléfono no es válido.");
            mensajeError.setVisible(true);
            return false;
        }

    }


    /**
     * Set cliente.
     *
     * @param cliente the cliente
     * @author Liling Xu
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * Set gestor.
     *
     * @param gestor the gestor
     * @author Liling Xu
     */
    public void setGestor(GestorPersistencia gestor) {
        this.gestor = gestor;
    }

    /**
     * Get cliente.
     *
     * @return the cliente
     * @author Liling Xu
     */
    public Cliente getCliente() {
        return cliente;
    }


    /**
     * Inicializa los TextFields con los datos del cliente seleccionado en la anterior ventana.
     * Hacer un binding de los TexFields, si hay un campo vacío no se podrá habilitar el botón.
     *
     * @author Liling Xu
     */
    public void iniciar() {
        dniLB.setText(cliente.getDni());
        nombreTF.setText(cliente.getNombre());
        telefonoTF.setText(String.valueOf(cliente.getTelefono()));
        calleTF.setText(cliente.getCalle());

        botonGuardar.disableProperty().bind(
                nombreTF.textProperty().isEmpty().or(
                        telefonoTF.textProperty().isEmpty()).or(
                        calleTF.textProperty().isEmpty()
                )
        );
    }
}
