package javafx.controller;

import clases.Cliente;
import clases.Pedido;
import clases_gestion_conexion.Conexion;
import clases_gestion_conexion.ConexionJDBC;
import clases_gestion_conexion.ConexionMongo;
import com.mongodb.client.MongoClient;
import gestion_excepcion.ConnexioException;
import gestor_persistencia.GestorPersistencia;
import gestor_persistencia.GestorPersistenciaJDBC;
import gestor_persistencia.GestorPersistenciaMongo;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import util.Util;

import java.io.IOException;
import java.sql.Connection;
import java.time.LocalDateTime;
import java.util.List;;

/**
 * The type Controller.
 *
 * @author Liling Xu y Bryan Oncoy
 */
public class Controller {

    public TextField campoDniCliente;
    private Conexion conexion;
    private String[] tipoConexion = {"JDBC"};
    private GestorPersistencia gestor = null;


    public TableView<Pedido> tablaPedidos;
    public TableColumn<Pedido, Integer> numPedidoColumna;
    public TableColumn<Pedido, Double> precioColumna;
    public TableColumn<Pedido, LocalDateTime> fechaColumna;
    public TableColumn<Pedido, Integer> caloriasColumna;
    public TableColumn<Pedido, String> productosColumna;
    public TableColumn<Pedido, Button> pagarColumna;

    public TableView<Cliente> tablaClientes;
    public TableColumn<Cliente, String> dniColumna;
    public TableColumn<Cliente, String> nombreColumna;
    public TableColumn<Cliente, Integer> telefonoColumna;
    public TableColumn<Cliente, String> calleColumna;
    public TableColumn<Cliente, Button> eliminarColumna;
    public TableColumn<Cliente, Button> editarColumna;
    public TextField campoDni;

    /**
     * Iniciar la conexión y las tablas.
     *
     * @author Liling Xu y Bryan Oncoy
     */
    public void initialize() {
        conectar();

        if (tipoConexion[0].equals("JDBC")) {
            gestor.crearTablaCliente();
            gestor.crearTablaPedido();
            gestor.crearTablaProducto();
            gestor.crearTablaProductoAlmacen();
        }

        numPedidoColumna.setCellValueFactory(new PropertyValueFactory<>("numPedido"));
        precioColumna.setCellValueFactory(new PropertyValueFactory<>("precioTotal"));
        fechaColumna.setCellValueFactory(new PropertyValueFactory<>("fechaPedido"));
        caloriasColumna.setCellValueFactory(new PropertyValueFactory<>("kcalTotal"));
        productosColumna.setCellValueFactory(new PropertyValueFactory<>("productos"));
        pagarColumna.setCellValueFactory(new PropertyValueFactory<>("botonPagar"));

        dniColumna.setCellValueFactory(new PropertyValueFactory<>("dni"));
        nombreColumna.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        telefonoColumna.setCellValueFactory(new PropertyValueFactory<>("telefono"));
        calleColumna.setCellValueFactory(new PropertyValueFactory<>("calle"));
        eliminarColumna.setCellValueFactory(new PropertyValueFactory<>("botonEliminar"));
        editarColumna.setCellValueFactory(new PropertyValueFactory<>("botonEditar"));

        tablaClientes.setPlaceholder(new Label("No se ha encontrado ningún cliente."));
        tablaPedidos.setPlaceholder(new Label("No se ha encontrado ningún pedido."));

        buscarPedidos();
        buscarClientes();
    }

    /**
     * Conecta la conexión según el tipo de conexión.
     *
     * @author Bryan Oncoy
     */
    public void conectar() {
        try {
            if (conexion != null && conexion.getConnection() != null)
                conexion.close();

            if (tipoConexion[0].equals("JDBC")) {
                conexion = new ConexionJDBC();
                conexion.conecta();
                gestor = new GestorPersistenciaJDBC((Connection) conexion.getConnection());
            } else if (tipoConexion[0].equals("Mongo")) {
                conexion = new ConexionMongo();
                conexion.conecta();
                gestor = new GestorPersistenciaMongo((MongoClient) conexion.getConnection());
            }
        } catch (ConnexioException e) {
            e.mostrarMensaje();
            crearVentanaConexion();
        }
    }

    /**
     * Busca el cliente en la BBDD por dni introducido en el buscador.
     *
     * @author Liling Xu
     */
    public void buscarClientePorDni(ActionEvent actionEvent) {
        Cliente cliente = gestor.recuperarClientePorDNI(campoDni.getText());

        tablaClientes.getItems().clear();
        if (campoDni.getText().length() == 0) {
            buscarClientes();
        } else {
            if (cliente != null) {
                String dni = cliente.getDni();
                cliente.getBotonEliminar().setOnAction(
                        event -> eliminarClientePorDni(dni)
                );

                tablaClientes.getItems().add(cliente);
            }
        }
    }

    /**
     * Busca todos los clientes que hay guardados en la BBDD.
     *
     * @author Liling Xu
     */
    public void buscarClientes() {
        List<Cliente> clientes = gestor.recuperarClientes();

        for (Cliente cliente : clientes) {
            cliente.getBotonEliminar().setOnAction(
                    event -> eliminarClientePorDni(cliente.getDni())
            );
            cliente.getBotonEditar().setOnAction(
                    event -> crearVentanaEditCliente(cliente)
            );
        }
        tablaClientes.setItems(FXCollections.observableList(clientes));
    }

    /**
     * Elimina el cliente en la BBDD que tenga el dni pasado por parametro.
     *
     * @param dni dni del cliente
     * @author Liling Xu
     */
    public void eliminarClientePorDni(String dni) {
        gestor.eliminarCliente(dni);

        buscarClientes();
        buscarPedidos();
    }

    /**
     * Busca todos los pedidos en la BBDD del cliente, con el dni introducido en el buscador.
     *
     * @param actionEvent the action event
     * @author Bryan Oncoy
     */
    public void buscarPedidosPorDni(ActionEvent actionEvent) {
        if (campoDniCliente.getText().length() == 0) {
            buscarPedidos();
        } else {
            List<Pedido> pedidos = gestor.recuperarPedidosPorDni(campoDniCliente.getText());
            if (pedidos != null) {
                for (Pedido pedido : pedidos) {
                    if (pedido.isPagado()) {
                        pedido.getBotonPagar().setDisable(true);
                    } else {
                        pedido.getBotonPagar().setOnAction(event -> {
                            gestor.actualizarPedido(pedido);
                            buscarPedidos();
                        });
                    }
                }
                tablaPedidos.setItems(FXCollections.observableList(pedidos));
            }
        }
    }

    /**
     * Lee todos los pedidos de la BBDD y crea la tabla con los pedidos leidos.
     *
     * @author Bryan Oncoy
     */
    public void buscarPedidos() {
        List<Pedido> pedidos = gestor.recuperarPedidos();

        for (Pedido pedido : pedidos) {
            if (pedido.isPagado()) {
                pedido.getBotonPagar().setDisable(true);
            } else {
                pedido.getBotonPagar().setOnAction(event -> {
                    gestor.actualizarPedido(pedido);
                    buscarPedidos();
                });
            }
        }

        tablaPedidos.setItems(FXCollections.observableList(pedidos));
    }

    /**
     * Crea una nueva ventana de editar cliente.
     *
     * @author Liling Xu
     */
    private void crearVentanaEditCliente(Cliente cliente) {
        try {
            FXMLLoader fxmlLoader = Util.getFxmlLoader("layoutEditarCliente");
            Parent root = fxmlLoader.load();

            ControllerEditarCliente editarCliente = fxmlLoader.getController();
            editarCliente.setCliente(cliente);
            editarCliente.setGestor(gestor);
            editarCliente.iniciar();

            Util.abrirVentana(root, 600, 400);

            buscarClientes();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Crea una nueva ventana de gestionar conexión.
     *
     * @author Bryan Oncoy
     */
    public void crearVentanaConexion() {
        try {
            FXMLLoader fxmlLoader = Util.getFxmlLoader("layoutConexion");
            Parent root = fxmlLoader.load();

            ControllerConexion controllerConexion = fxmlLoader.getController();
            controllerConexion.setTipoConexion(tipoConexion);

            Util.abrirVentana(root, 600, 400);

            conectar();

            buscarClientes();
            buscarPedidos();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Crea una nueva ventana de crear nuevo pedido.
     *
     * @param actionEvent the action event
     * @author Bryan Oncoy
     */
    public void crearVentanaNuevoPedido(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = Util.getFxmlLoader("layoutLogin");
            Parent root = fxmlLoader.load();

            ControllerLogin controllerLogin = fxmlLoader.getController();
            controllerLogin.setGestor(gestor);

            Util.abrirVentana(root, 800, 600);
            buscarPedidos();
            buscarClientes();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Crea una nueva ventana de sección de productos.
     *
     * @param actionEvent the action event
     * @author Liling Xu
     */
    public void crearVentanaProductos(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = Util.getFxmlLoader("layoutProducto");
            Parent root = fxmlLoader.load();

            ControllerProducto controllerProducto = fxmlLoader.getController();
            controllerProducto.setGestor(gestor);
            controllerProducto.buscarProductos();

            Util.abrirVentana(root, 900, 700);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
