package javafx.controller;

import clases.Cliente;
import gestion_excepcion.ClauException;
import gestor_persistencia.GestorPersistencia;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import util.Util;

/**
 * The type Controller registro.
 *
 * @author Bryan Oncoy
 */
public class ControllerRegistro {
    private Cliente cliente;
    private GestorPersistencia gestor;

    public TextField campoDNI;
    public TextField campoNombre;
    public TextField campoTelefono;
    public TextField campoCalle;

    public Button botonRegistrar;
    public Label mensajeTelefonoInvalido;
    public Label mensajeDniRegistrado;

    /**
     * Hace un binding con los TexFields, si hay un campo vacío no se podrá habilitar el botón.
     *
     * @author Bryan Oncoy
     */
    public void initialize() {
        botonRegistrar.disableProperty().bind(
                campoDNI.textProperty().isEmpty().or(
                        campoNombre.textProperty().isEmpty()).or(
                        campoTelefono.textProperty().isEmpty()).or(
                        campoCalle.textProperty().isEmpty())
        );
    }

    /**
     * Sets gestor.
     *
     * @param gestor the gestor
     * @author Bryan Oncoy
     */
    public void setGestor(GestorPersistencia gestor) {
        this.gestor = gestor;
    }

    /**
     * Guarda el cliente a la BBDD, si el telefono es valido y que no exísta el dni del cliente en la BBDD.
     *
     * @param actionEvent the action event
     * @author Bryan Oncoy
     */
    public void registrarCliente(ActionEvent actionEvent) {
        mensajeDniRegistrado.setVisible(false);
        mensajeTelefonoInvalido.setVisible(false);

        cliente = gestor.recuperarClientePorDNI(campoDNI.getText());
        if (cliente == null) {
            if (Util.comprobarTelefono(campoTelefono.getText())) {
                cliente = new Cliente(campoDNI.getText(), campoNombre.getText(), Integer.parseInt(campoTelefono.getText()), campoCalle.getText());
                try {
                    gestor.insertarCliente(cliente);
                } catch (ClauException e) {
                    System.out.println(e.getMessage());
                }
                Util.cambiarEscena("layoutPedido", cliente, gestor, actionEvent);
            } else {
                mensajeTelefonoInvalido.setVisible(true);
            }
        } else {
            mensajeDniRegistrado.setVisible(true);
        }
    }

    /**
     * Volver al escena login.
     *
     * @param actionEvent the action event
     * @author Bryan Oncoy
     */
    public void volverALogin(ActionEvent actionEvent) {
        Util.cambiarEscena("layoutLogin", cliente, gestor, actionEvent);
    }
}
