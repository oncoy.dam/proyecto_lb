package javafx.controller;

import javafx.App;
import javafx.event.ActionEvent;

import java.io.IOException;
/**
 * @author Bryan Oncoy
 */
public class ControllerPortada {
    /**
     * Cambio de escena del 'layoutPortada' al 'layout'
     */
    public void cambiarEscenaPrincipal(ActionEvent actionEvent) {
        try {
            App.setRoot("layout");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
