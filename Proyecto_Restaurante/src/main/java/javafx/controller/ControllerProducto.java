package javafx.controller;

import clases.Producto;
import gestor_persistencia.GestorPersistencia;
import javafx.App;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import util.Util;

import java.io.IOException;
import java.util.List;

/**
 * The type Controller producto.
 *
 * @author Bryan Oncoy
 */
public class ControllerProducto {
    public TableView<Producto> tablaProductos;
    public TableColumn<Producto, Integer> idColumna;
    public TableColumn<Producto, String> nombreColumna;
    public TableColumn<Producto, Integer> caloriasColumna;
    public TableColumn<Producto, Boolean> glutenColumna;
    public TableColumn<Producto, Double> precioColumna;
    public TableColumn<Producto, Button> editarColumna;
    public TableColumn<Producto, Button> eliminarColumna;

    private GestorPersistencia gestor;

    /**
     * Initializa la tabla de productos.
     *
     * @author Bryan Oncoy
     */
    public void initialize() {
        idColumna.setCellValueFactory(new PropertyValueFactory<>("id"));
        nombreColumna.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        caloriasColumna.setCellValueFactory(new PropertyValueFactory<>("calorias"));
        glutenColumna.setCellValueFactory(new PropertyValueFactory<>("gluten"));
        precioColumna.setCellValueFactory(new PropertyValueFactory<>("precio"));
        editarColumna.setCellValueFactory(new PropertyValueFactory<>("botonEditar"));
        eliminarColumna.setCellValueFactory(new PropertyValueFactory<>("botonEliminar"));

        tablaProductos.setPlaceholder(new Label("No se ha encontrado ningún producto."));
    }

    /**
     * Sets gestor.
     *
     * @param gestor the gestor
     * @author Bryan Oncoy
     */
    public void setGestor(GestorPersistencia gestor) {
        this.gestor = gestor;
    }

    /**
     * Busca todos los Productos que hay guardados en la BBDD y lo muestra en la tabla.
     *
     * @author Bryan Oncoy
     */
    public void buscarProductos() {
        List<Producto> productos = gestor.recuperarProductosAlmacen();

        for (Producto producto : productos) {
            producto.getBotonEditar().setOnAction(
                    event -> crearVentanaEditProducto(producto)
            );

            producto.getBotonEliminar().setOnAction(
                    event -> eliminarProductoPorId(producto.getId())
            );
        }

        tablaProductos.setItems(FXCollections.observableList(productos));
    }

    /**
     * Crear ventana edit producto.
     *
     * @param producto the producto
     * @author Bryan Oncoy
     */
    public void crearVentanaEditProducto(Producto producto) {
        try {
            FXMLLoader fxmlLoader = Util.getFxmlLoader("layoutEditarProducto");

            Parent root = fxmlLoader.load();

            ControllerEditarProducto controllerProducto = fxmlLoader.getController();
            controllerProducto.setProducto(producto);
            controllerProducto.setGestor(gestor);
            controllerProducto.inicializar();

            Util.abrirVentana(root, 600, 400);

            buscarProductos();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Elimina el producto por id introducido por parametro.
     *
     * @param id the id
     * @author Bryan Oncoy
     */
    public void eliminarProductoPorId(int id) {
        gestor.eliminarProductoAlmacen(id);

        buscarProductos();
    }

    /**
     * Crear ventana nuevo producto.
     *
     * @param actionEvent the action event
     * @author Bryan Oncoy
     */
    public void crearVentanaNuevoProducto(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = Util.getFxmlLoader("layoutNuevoProducto");

            Parent root = fxmlLoader.load();

            ControllerNuevoProducto nuevoProducto = fxmlLoader.getController();
            nuevoProducto.setGestor(gestor);

            Util.abrirVentana(root, 600, 400);

            buscarProductos();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}