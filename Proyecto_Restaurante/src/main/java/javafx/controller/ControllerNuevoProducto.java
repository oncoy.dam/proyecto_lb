package javafx.controller;

import clases.Producto;
import gestion_excepcion.ClauException;
import gestor_persistencia.GestorPersistencia;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;
import util.Util;

/**
 * The type Controller nuevo producto.
 *
 * @author Liling Xu
 */
public class ControllerNuevoProducto {
    private GestorPersistencia gestor;
    private Producto producto;
    private ToggleGroup group;

    public TextField kcalTF;
    public TextField precioTF;
    public TextField nombreTF;

    public RadioButton conGluten;
    public RadioButton sinGluten;

    public Button botonGuardar;
    public Label mensajeErrorPrecio;
    public Label mensajeErrorCalorias;
    public Label mensajeErrorNombre;

    /**
     * Sets gestor.
     *
     * @param gestor the gestor
     * @author Liling Xu
     */
    public void setGestor(GestorPersistencia gestor) {
        this.gestor = gestor;
    }

    /**
     * Inicializa el ToggleGroup con los dos RadioButtons
     * Hace un binding con los TexFields, si hay un campo vacío no se podrá habilitar el botón.
     *
     * @author Liling Xu
     */
    public void initialize() {
        conGluten.setSelected(true);

        group = new ToggleGroup();
        conGluten.setToggleGroup(group);
        sinGluten.setToggleGroup(group);

        botonGuardar.disableProperty().bind(
                nombreTF.textProperty().isEmpty().or(
                        kcalTF.textProperty().isEmpty()).or(
                        precioTF.textProperty().isEmpty()));
    }

    /**
     * Guarda el nuevo producto, si los campos són válidos.
     *
     * @param actionEvent the action event
     * @author Liling Xu
     */
    public void guardarProducto(ActionEvent actionEvent) {
        if (controlarErrores()) {
            producto = new Producto();

            producto.setId(gestor.getNextNumProductoAlmacen());
            producto.setNombre(nombreTF.getText());
            producto.setCalorias(Integer.parseInt(kcalTF.getText()));
            producto.setGluten(conGluten.isSelected());

            double precio = Math.round(Double.parseDouble(precioTF.getText()) * 100) / 100.0;
            producto.setPrecio(precio);

            try {
                gestor.insertarProductoAlmacen(producto);
            } catch (ClauException e) {
                System.out.println(e.getMessage());
            }

            Util.cerrarVentana(actionEvent);
        }
    }

    /**
     * Controla campos que no son válidos.
     *
     * @return true, si los campos son válidos  -  false, si un campo no és válido
     * @author Liling Xu
     */
    public boolean controlarErrores() {
        boolean correcto = true;
        mensajeErrorNombre.setVisible(false);
        mensajeErrorCalorias.setVisible(false);
        mensajeErrorPrecio.setVisible(false);

        if (gestor.buscarProductoAlmacenPorNombre(nombreTF.getText())) {
            mensajeErrorNombre.setVisible(true);
            correcto = false;
        }

        try {
            Integer.parseInt(kcalTF.getText());
        } catch (NumberFormatException e) {
            mensajeErrorCalorias.setVisible(true);
            correcto = false;
        }

        try {
            Double.parseDouble(precioTF.getText());
        } catch (NumberFormatException e) {
            mensajeErrorPrecio.setVisible(true);
            correcto = false;
        }

        return correcto;
    }


}
