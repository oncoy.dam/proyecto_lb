package javafx.controller;

import clases.Cliente;
import gestor_persistencia.GestorPersistencia;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import util.Util;

/**
 * The type Controller login.
 *
 * @author Bryan Oncoy
 */
public class ControllerLogin {
    private Cliente cliente;
    private GestorPersistencia gestor;

    public TextField campoDNILogin;
    public Label mensajeDniNoRegistrado;

    /**
     * Sets gestor.
     *
     * @param gestor the gestor
     * @author Bryan Oncoy
     */
    public void setGestor(GestorPersistencia gestor) {
        this.gestor = gestor;
    }

    /**
     * Abre la ventana registro.
     *
     * @param actionEvent the action event
     * @author Bryan Oncoy
     */
    public void abrirVentanaRegistro(ActionEvent actionEvent) {
        Util.cambiarEscena("layoutRegistro", cliente, gestor, actionEvent);
    }

    /**
     * Login cliente, si el cliente existe cambia de escena, si no, muestra el mensaje de error
     *
     * @param actionEvent the action event
     * @author Bryan Oncoy
     */
    public void loginCliente(ActionEvent actionEvent) {
        cliente = gestor.recuperarClientePorDNI(campoDNILogin.getText());
        if (cliente == null) {
            mensajeDniNoRegistrado.setVisible(true);
        } else {
            Util.cambiarEscena("layoutPedido", cliente, gestor, actionEvent);
        }
    }

}
