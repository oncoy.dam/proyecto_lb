package javafx.controller;

import clases.Producto;
import gestor_persistencia.GestorPersistencia;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import util.Util;

/**
 * The type Controller editar producto.
 *
 * @author Bryan Oncoy
 */
public class ControllerEditarProducto {
    private GestorPersistencia gestor;
    private Producto producto;
    private ToggleGroup group;

    public Label idLB;
    public Label nombreLB;
    public TextField kcalTF;
    public RadioButton conGluten;
    public RadioButton sinGluten;
    public TextField precioTF;

    public Label errorKcal;
    public Label errorPrecio;
    public Button botonGuardar;

    /**
     * Sets gestor.
     *
     * @param gestor the gestor
     * @author Bryan Oncoy
     */
    public void setGestor(GestorPersistencia gestor) {
        this.gestor = gestor;
    }

    /**
     * Sets producto.
     *
     * @param producto the producto
     * @author Bryan Oncoy
     */
    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    /**
     * Guarda el producto con los cambios realizados, si los campos són válidos.
     *
     * @param actionEvent the action event
     * @author Liling Xu
     */
    public void guardarCambiosProducto(ActionEvent actionEvent) {
        if (controlarErrores()) {
            producto.setCalorias(Integer.parseInt(kcalTF.getText()));
            producto.setGluten(conGluten.isSelected());
            producto.setPrecio(Double.parseDouble(precioTF.getText()));

            gestor.actualizarProductoAlmacen(producto);

            Util.cerrarVentana(actionEvent);
        }
    }

    /**
     *Valida los campos introducidos.
     *
     * @return true, si los campos son válidos  -  false, si un campo no és válido
     * @author Liling XU
     */
    public boolean controlarErrores() {
        boolean correcto = true;
        errorKcal.setVisible(false);
        errorPrecio.setVisible(false);

        try {
            Integer.parseInt(kcalTF.getText());
        } catch (NumberFormatException e) {
            errorKcal.setVisible(true);
            correcto = false;
        }

        try {
            Double.parseDouble(precioTF.getText());
        } catch (NumberFormatException e) {
            errorPrecio.setVisible(true);
            correcto = false;
        }

        return correcto;
    }

    /**
     * Inicializa los TextFields con los datos del producto seleccionado en la ventana anterior.
     * Hace un binding de los TexFields, si hay un campo vacío no se podrá habilitar el botón.
     * @author Bryan Oncoy
     */
    public void inicializar() {
        idLB.setText(String.valueOf(producto.getId()));
        nombreLB.setText(producto.getNombre());
        kcalTF.setText(String.valueOf(producto.getCalorias()));
        conGluten.setToggleGroup(group);
        sinGluten.setToggleGroup(group);
        group = new ToggleGroup();
        if (producto.isGluten())
            conGluten.setSelected(true);
        else
            sinGluten.setSelected(true);
        precioTF.setText(String.valueOf(producto.getPrecio()));

        botonGuardar.disableProperty().bind(kcalTF.textProperty().isEmpty().or(precioTF.textProperty().isEmpty()));
    }
}