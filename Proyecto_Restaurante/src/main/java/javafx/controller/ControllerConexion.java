package javafx.controller;

import clases_gestion_conexion.Conexion;
import clases_gestion_conexion.ConexionJDBC;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import util.PropertyFile;
import util.Util;

/**
 * The type Controller conexion.
 *
 * @author Liling Xu y Bryan Oncoy
 */
public class ControllerConexion {
    private String[] tipoConexion;
    private ToggleGroup grupo;
    @FXML
    private RadioButton conJDBC;
    @FXML
    private RadioButton conMongo;
    public TextField campoURL;
    public TextField campoUsuario;
    public TextField campoContrasenya;

    /**
     * Inicializa el ToggleGroup con los RadioButtons
     *
     * @author Liling Xu
     */
    public void initialize() {
        grupo = new ToggleGroup();
        conJDBC.setToggleGroup(grupo);
        conMongo.setToggleGroup(grupo);

        setCampos();

        campoURL.disableProperty().bind(conMongo.selectedProperty());
        campoUsuario.disableProperty().bind(conMongo.selectedProperty());
        campoContrasenya.disableProperty().bind(conMongo.selectedProperty());
    }

    /**
     * Cambia conexion según el tipo seleccionado.
     *
     * @param actionEvent the action event
     * @author Bryan Oncoy
     */
    public void cambiarConexion(ActionEvent actionEvent) {
        if (grupo.getSelectedToggle() == conJDBC) {
            tipoConexion[0] = "JDBC";
            ConexionJDBC.setUrl(campoURL.getText());
            ConexionJDBC.setUsuari(campoUsuario.getText());
            ConexionJDBC.setContrasenya(campoContrasenya.getText());
        } else {
            tipoConexion[0] = "Mongo";
        }

        Util.cerrarVentana(actionEvent);
    }

    /**
     * Set tipo conexion.
     *
     * @param tipoConexion the tipo conexion
     * @author Liling Xu
     */
    public void setTipoConexion(String[] tipoConexion) {
        this.tipoConexion = tipoConexion;
    }

    public void setCampos() {
        campoURL.setText(ConexionJDBC.getUrl());
        campoUsuario.setText(ConexionJDBC.getUsuari());
        campoContrasenya.setText(ConexionJDBC.getContrasenya());
    }
}
