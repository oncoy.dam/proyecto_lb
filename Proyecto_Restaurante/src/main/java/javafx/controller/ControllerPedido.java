package javafx.controller;

import clases.Cliente;
import clases.Pedido;
import clases.Producto;
import gestion_excepcion.ClauException;
import gestor_persistencia.GestorPersistencia;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import util.Util;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Controller pedido.
 *
 * @author Bryan Oncoy
 */
public class ControllerPedido {
    public Button botonPagarAhora;
    public Button botonPagarMasTarde;
    public Button botonAnadir;
    public Button botonQuitar;
    private GestorPersistencia gestor;
    private Cliente cliente;
    private DecimalFormat decimal = new DecimalFormat("0.00");

    public List<Producto> listaProductos = new ArrayList<>();
    public TableView<Producto> tablaProductos;
    public TableColumn<Producto, String> nombreColumna;
    public TableColumn<Producto, Double> precioColumna;

    public TableView<Producto> tablaProdPedidos;
    public TableColumn<Producto, String> productoColumna;

    public Label precioTotal;

    /**
     * Hace un binding de los botones '+' y '-', si no hay producto selecionado, los botones no se habilitarán..
     * Hace un binding de los botones 'pagar ahora' y 'pagar despues', si no hay productos selecionados en el pedido, los botones no se habilitarán.
     * Inicializa las dos tablas.
     *
     * @author Bryan Oncoy
     */
    public void initialize() {
        botonAnadir.disableProperty().bind(tablaProductos.getSelectionModel().selectedItemProperty().isNull());
        botonQuitar.disableProperty().bind(tablaProdPedidos.getSelectionModel().selectedItemProperty().isNull());

        botonPagarAhora.disableProperty().bind(Bindings.isEmpty(tablaProdPedidos.getItems()));
        botonPagarMasTarde.disableProperty().bind(Bindings.isEmpty(tablaProdPedidos.getItems()));

        nombreColumna.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        precioColumna.setCellValueFactory(new PropertyValueFactory<>("precio"));
        productoColumna.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        tablaProductos.setPlaceholder(new Label("No se ha encontrado ningún producto"));
        tablaProdPedidos.setPlaceholder(new Label("    Añade algún\nproducto a tu pedido."));
    }

    /**
     * Inicializar.
     *
     * @author Bryan Oncoy
     */
    public void inicializar() {
        List<Producto> productos = gestor.recuperarProductosAlmacen();
        tablaProductos.setItems(FXCollections.observableList(productos));
    }

    /**
     * Sets cliente.
     *
     * @param cliente the cliente
     * @author Bryan Oncoy
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * Sets gestor.
     *
     * @param gestor the gestor
     * @author Bryan Oncoy
     */
    public void setGestor(GestorPersistencia gestor) {
        this.gestor = gestor;
        inicializar();
    }

    /**
     * Añade el producto selecionado a la lista de productos.
     * Muestra el precio total.
     *
     * @param actionEvent the action event
     * @author Bryan Oncoy
     */
    public void anadirProducto(ActionEvent actionEvent) {
        Producto producto = tablaProductos.getSelectionModel().getSelectedItem();

        listaProductos.add(producto);
        tablaProdPedidos.getItems().add(producto);

        double precio = formatPrecio(precioTotal.getText());
        precio += producto.getPrecio();

        precioTotal.setText(decimal.format(precio));
    }

    /**
     * Quita el producto selecionado a la lista de productos.
     * Muestra el precio total.
     *
     * @author Bryan Oncoy
     */
    public void quitarProducto() {
        Producto producto = tablaProdPedidos.getSelectionModel().getSelectedItem();
        listaProductos.remove(producto);
        tablaProdPedidos.getItems().remove(producto);

        double precio = formatPrecio(precioTotal.getText());
        precio -= producto.getPrecio();

        precioTotal.setText(decimal.format(precio));
    }

    /**
     * Cambia el precio total con el formato double
     *
     * @param precio the precio
     * @return double double
     * @author Bryan Oncoy
     */
    public double formatPrecio(String precio) {
        String[] cadena = precio.split(",");
        String precioDouble = cadena[0] + "." + cadena[1];
        return Double.parseDouble(precioDouble);
    }

    /**
     * Pagar ahora, se guarda a la BBDD un nuevo pedido con parametro 'pagado'=true.
     *
     * @param actionEvent the action event
     * @author Bryan Oncoy
     */
    public void pagarAhora(ActionEvent actionEvent) {
        pagar(true, actionEvent);
    }

    /**
     * Pagar mas tarde, se guarda a la BBDD un nuevo pedido con parametro 'pagado'=false.
     *
     * @param actionEvent the action event
     * @author Bryan Oncoy
     */
    public void pagarMasTarde(ActionEvent actionEvent) {
        pagar(false, actionEvent);
    }

    /**
     * Inserta un nuevo pedido a la BBDD
     *
     * @param pagar       the pagar
     * @param actionEvent the action event
     * @author Bryan Oncoy
     */
    public void pagar(boolean pagar, ActionEvent actionEvent) {
        Pedido pedido = new Pedido(gestor.getNextNumPedido(), LocalDateTime.now(), listaProductos, pagar, cliente.getDni());
        try {
            gestor.insertarPedido(pedido);
        } catch (ClauException e) {
            System.out.println(e.getMessage());
        }

        Util.cerrarVentana(actionEvent);
    }
}
