package gestion_excepcion;

import java.sql.SQLIntegrityConstraintViolationException;

/**
 * @author Bryan Oncoy
 */
public class ClauException extends Exception{
    String message;

    public ClauException(Exception e) {
        super();
        if (e.getClass() == SQLIntegrityConstraintViolationException.class) {
            message = "Clave duplicada. No se ha podido guardar en la base de datos.";
        }
    }

    @Override
    public String getMessage() {
        return message;
    }

}
