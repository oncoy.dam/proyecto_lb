package gestion_excepcion;

import javafx.scene.control.Alert;

/**
 * @author Bryan Oncoy
 */
public class ConnexioException extends Exception {

    @Override
    public String getMessage() {
        return "No se ha podido establecer la conexión con la base de datos.";
    }

    public void mostrarMensaje() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Error en la conexión.");
        alert.setHeaderText(null);
        alert.setContentText("Se ha producido un error al intentar conectarse a la base de datos.\n" +
                "Configure correctamente los parámetros de conexión.");
        alert.showAndWait();
    }
}
