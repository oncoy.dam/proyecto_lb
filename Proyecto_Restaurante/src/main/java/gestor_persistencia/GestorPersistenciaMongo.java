package gestor_persistencia;

import clases_mongodb.ClienteMongoDB;
import clases_mongodb.PedidoMongoDB;
import clases_mongodb.ProductoAlmacenMongoDB;
import com.mongodb.client.MongoClient;
/**
 * @author Bryan Oncoy
 */
public class GestorPersistenciaMongo extends GestorPersistencia{
    public GestorPersistenciaMongo(MongoClient con) {
        daoCliente = new ClienteMongoDB(con);
        daoPedido = new PedidoMongoDB(con);
        daoProductoAlmacen = new ProductoAlmacenMongoDB(con);
    }
}
