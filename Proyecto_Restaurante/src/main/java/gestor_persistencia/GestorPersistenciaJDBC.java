package gestor_persistencia;

import clases_jdbc.ClienteJDBC;
import clases_jdbc.PedidoJDBC;
import clases_jdbc.ProductoAlmacen;
import clases_jdbc.ProductoJDBC;

import java.sql.Connection;
/**
 * @author Bryan Oncoy
 */
public class GestorPersistenciaJDBC extends GestorPersistencia {
    public GestorPersistenciaJDBC(Connection con) {
        daoCliente = new ClienteJDBC(con);
        daoPedido = new PedidoJDBC(con);
        daoProducto = new ProductoJDBC(con);
        daoProductoAlmacen = new ProductoAlmacen(con);
    }
}
