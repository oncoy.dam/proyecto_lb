package gestor_persistencia;

import clases.Cliente;
import clases.Pedido;
import clases.Producto;
import dao.DAOCliente;
import dao.DAOPedido;
import dao.DAOProducto;
import dao.DAOProductoAlmacen;
import gestion_excepcion.ClauException;

import java.util.List;
/**
 * @author Bryan Oncoy
 */
public abstract class GestorPersistencia {
    protected DAOCliente daoCliente;
    protected DAOPedido daoPedido;
    protected DAOProducto daoProducto;
    protected DAOProductoAlmacen daoProductoAlmacen;

    //Cliente
    public void crearTablaCliente() {
        daoCliente.crearTabla();
    }

    public void insertarCliente(Cliente cliente) throws ClauException {
        daoCliente.insertar(cliente);
    }

    public void eliminarCliente(String dniCliente){
        daoCliente.eliminar(dniCliente);
    }

    public void actualizarCliente(Cliente cliente){
        daoCliente.actualitzar(cliente);
    }

    public List<Cliente> recuperarClientes() {
        return daoCliente.recuperar();
    }

    public Cliente recuperarClientePorDNI(String dniCliente) {
        return daoCliente.recuperarPerId(dniCliente);
    }


    //Pedido
    public void crearTablaPedido() {
        daoPedido.crearTabla();
    }

    public int getNextNumPedido() {
        return daoPedido.getNextNumPedido();
    }

    public void insertarPedido(Pedido pedido) throws ClauException {
        daoPedido.insertar(pedido);
    }

    public void eliminarPedido(Integer idPedido) {
        daoPedido.eliminar(idPedido);
    }

    public void actualizarPedido(Pedido pedido) {
        daoPedido.actualitzar(pedido);
    }

    public List<Pedido> recuperarPedidos() {
        return daoPedido.recuperar();
    }

    public Pedido recuperarPedidoPorId(Integer idPedido) {
        return daoPedido.recuperarPerId(idPedido);
    }

    public List<Pedido> recuperarPedidosPorDni(String dniCliente) {
        return daoPedido.recuperarPerDni(dniCliente);
    }

    public List<Pedido> recuperarPedidoSinPagar() {
        return daoPedido.buscarSinPagar();
    }

    //Producto
    public void crearTablaProducto() {
        daoProducto.crearTabla();
    }


    //ProductoAlmacen
    public void crearTablaProductoAlmacen() {
        daoProductoAlmacen.crearTabla();
    }

    public void insertarProductoAlmacen(Producto producto) throws ClauException {
        daoProductoAlmacen.insertar(producto);
    }

    public void eliminarProductoAlmacen(Integer idProducto) {
        daoProductoAlmacen.eliminar(idProducto);
    }

    public void actualizarProductoAlmacen(Producto producto) {
        daoProductoAlmacen.actualitzar(producto);
    }

    public List<Producto> recuperarProductosAlmacen() {
        return daoProductoAlmacen.recuperar();
    }

    public Producto recuperarProductoAlmacenPorId(Integer idProducto) {
        return daoProductoAlmacen.recuperarPerId(idProducto);
    }

    public boolean buscarProductoAlmacenPorNombre(String nombreCliente) {
        return daoProductoAlmacen.buscarPorNombre(nombreCliente);
    }

    public int getNextNumProductoAlmacen(){
        return daoProductoAlmacen.getNextNumProducto();
    }


}