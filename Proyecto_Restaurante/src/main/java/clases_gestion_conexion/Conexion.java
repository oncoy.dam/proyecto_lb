package clases_gestion_conexion;

import gestion_excepcion.ConnexioException;

/**
 * @author Bryan Oncoy
 */
public interface Conexion<T> {

    void conecta() throws ConnexioException;

    void close() throws ConnexioException;

    T getConnection();
}
