package clases_gestion_conexion;

import gestion_excepcion.ConnexioException;
import util.PropertyFile;
import util.Util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 * @author Bryan Oncoy
 */
public class ConexionJDBC implements Conexion<Connection> {
    private static String url = PropertyFile.getUrlJDBC();
    private static String usuari = PropertyFile.getUserJDBC();
    private static String contrasenya = PropertyFile.getPasswordJDBC();

    private Connection con;

    @Override
    public void conecta() throws ConnexioException {
        try {
            con = DriverManager.getConnection(url, usuari, contrasenya);
            System.out.println("Conexión abierta correctamente.");
        } catch (SQLException e) {
            Util.saveLogs(e);
            throw new ConnexioException();
        }
    }

    @Override
    public void close(){
        try {
            con.close();
            System.out.println("Conexión cerrada correctamente.");
        } catch (SQLException e) {
            Util.saveLogs(e);
        }
    }

    @Override
    public Connection getConnection() {
        return con;
    }

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        ConexionJDBC.url = url;
    }

    public static String getUsuari() {
        return usuari;
    }

    public static void setUsuari(String usuari) {
        ConexionJDBC.usuari = usuari;
    }

    public static String getContrasenya() {
        return contrasenya;
    }

    public static void setContrasenya(String contrasenya) {
        ConexionJDBC.contrasenya = contrasenya;
    }
}
