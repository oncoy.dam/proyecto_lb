package clases_gestion_conexion;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import gestion_excepcion.ConnexioException;
import util.Util;

/**
 * @author Bryan Oncoy
 */
public class ConexionMongo implements Conexion<MongoClient> {
    private MongoClient mongoClient;

    @Override
    public void conecta() throws ConnexioException {
        try {
            mongoClient = MongoClients.create();
        } catch (Exception e) {
            Util.saveLogs(e);
            throw new ConnexioException();
        }
    }

    @Override
    public void close() {
        mongoClient.close();

    }

    @Override
    public MongoClient getConnection() {
        return mongoClient;
    }
}