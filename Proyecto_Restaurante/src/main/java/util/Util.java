package util;

import clases.Cliente;
import gestion_excepcion.ConnexioException;
import gestor_persistencia.GestorPersistencia;
import javafx.App;
import javafx.controller.ControllerLogin;
import javafx.controller.ControllerPedido;
import javafx.controller.ControllerRegistro;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * The type Util.
 *
 * @author Bryan Oncoy
 */
public class Util {

    /**
     * Gets fxml loader.
     *
     * @param fxml the fxml
     * @return the fxml loader
     * @throws IOException the io exception
     */
    public static FXMLLoader getFxmlLoader(String fxml) throws IOException {
        return new FXMLLoader(App.class.getResource("forms/" + fxml + ".fxml"));
    }

    /**
     * Abrir ventana.
     *
     * @param root the root
     * @param v    the v
     * @param v1   the v 1
     */
    public static void abrirVentana(Parent root, int v, int v1) {
        Stage stage = new Stage();
        Scene scene = new Scene(root, v, v1);

        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
    }

    /**
     * Cerrar ventana.
     *
     * @param actionEvent the action event
     */
    public static void cerrarVentana(ActionEvent actionEvent) {
        Node source = (Node) actionEvent.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }


    /**
     * Comprobar telefono boolean.
     *
     * @param telefono the telefono
     * @return the boolean
     */
    public static boolean comprobarTelefono(String telefono) {
        if (telefono.length() == 9) {
            try {
                Integer.parseInt(telefono);
                return true;
            } catch (NumberFormatException e) {
                System.out.println("Teléfono incorrecto");
            }
        }
        return false;
    }

    /**
     * Cambiar escena.
     *
     * @param layout      the layout
     * @param cliente     the cliente
     * @param gestor      the gestor
     * @param actionEvent the action event
     */
    public static void cambiarEscena(String layout, Cliente cliente, GestorPersistencia gestor, ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = getFxmlLoader(layout);
            Parent root = fxmlLoader.load();

            switch (layout) {
                case "layoutPedido":
                    ControllerPedido pedido = fxmlLoader.getController();
                    pedido.setGestor(gestor);
                    pedido.setCliente(cliente);
                    break;
                case "layoutRegistro":
                    ControllerRegistro registro = fxmlLoader.getController();
                    registro.setGestor(gestor);
                    break;
                case "layoutLogin":
                    ControllerLogin login = fxmlLoader.getController();
                    login.setGestor(gestor);
                    break;
            }

            ((Node) actionEvent.getSource()).getScene().setRoot(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param e
     */
    public static void saveLogs(Exception e) {
        Logger logger = Logger.getLogger(e.getClass().getName());
        FileHandler handler;
        try {
            handler = new FileHandler("logFiles.log", true);
            SimpleFormatter formatter = new SimpleFormatter();
            handler.setFormatter(formatter);
            logger.addHandler(handler);
        } catch (IOException ignored) {
        }

        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));

        logger.log(Level.WARNING, sw.toString());
    }
}
