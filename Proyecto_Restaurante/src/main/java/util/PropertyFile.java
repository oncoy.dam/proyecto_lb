package util;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
/**
 * @author Liling Xu
 */
public class PropertyFile {
    public static Properties p = new Properties();

    public static void inicializar() throws IOException {
        p.load(new FileReader("./src/main/java/util/config.properties"));
    }

    public static String getDatabase() {
        return p.getProperty("database");

    }

    public static String getUrlJDBC() {
        return p.getProperty("urlJDBC");

    }

    public static String getUserJDBC() {
        return p.getProperty("userJDBC");

    }

    public static String getPasswordJDBC() {
        return p.getProperty("passwordJDBC");

    }

}
