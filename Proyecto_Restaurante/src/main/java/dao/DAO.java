package dao;

import gestion_excepcion.ClauException;

import java.util.List;
/**
 * @author Liling Xu
 */
public interface DAO<T, I> {
    void crearTabla();

    void insertar(T t) throws ClauException;

    void eliminar(I i);

    void actualitzar(T t);

    List<T> recuperar();

    T recuperarPerId(I i);
}
