package dao;

import clases.Producto;
import gestion_excepcion.ClauException;

import java.util.List;
/**
 * @author Liling Xu
 */
public interface DAOProductoAlmacen extends DAO<Producto, Integer> {
    @Override
    void crearTabla();

    @Override
    void insertar(Producto producto) throws ClauException;

    @Override
    void eliminar(Integer idProducto);

    @Override
    void actualitzar(Producto producto);

    @Override
    List<Producto> recuperar();

    @Override
    Producto recuperarPerId(Integer idAlimento);

    boolean buscarPorNombre(String nombreProducto);

    int getNextNumProducto();
}
