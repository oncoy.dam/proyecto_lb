package dao;

import clases.Pedido;
import gestion_excepcion.ClauException;

import java.util.List;
/**
 * @author Liling Xu
 */
public interface DAOPedido extends DAO<Pedido, Integer> {
    @Override
    void crearTabla();

    @Override
    void insertar(Pedido pedido) throws ClauException;

    @Override
    void eliminar(Integer idPedido);

    @Override
    void actualitzar(Pedido pedido);

    @Override
    List<Pedido> recuperar();

    @Override
    Pedido recuperarPerId(Integer idPedido);

    int getNextNumPedido();

    List<Pedido> recuperarPerDni(String dniCliente);

    List<Pedido> buscarSinPagar();

}