package dao;

import clases.Cliente;
import gestion_excepcion.ClauException;

import java.util.List;
/**
 * @author Liling Xu
 */
public interface DAOCliente extends DAO<Cliente, String>{
    @Override
    void crearTabla();

    @Override
    void insertar(Cliente cliente) throws ClauException;

    @Override
    void eliminar(String dniCliente);

    @Override
    void actualitzar(Cliente cliente);

    @Override
    List<Cliente> recuperar();

    @Override
    Cliente recuperarPerId(String dniCliente);
}
