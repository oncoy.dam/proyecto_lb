package dao;

import clases.Producto;
import gestion_excepcion.ClauException;

import java.util.List;
/**
 * @author Bryan Oncoy
 */
public interface DAOProducto {
    void crearTabla();

    void insertar(List<Producto> productos, Integer numPedido) throws ClauException;

    void eliminarPorNumPedido(Integer numPedido);

    List<Producto> recuperarPorNumPedido(Integer numPedido);
}
