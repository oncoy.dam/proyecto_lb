package clases_jdbc;

import clases.Producto;
import dao.DAOProducto;
import dao.DAOProductoAlmacen;
import gestion_excepcion.ClauException;
import util.Util;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
/**
 * @author Bryan Oncoy
 */
public class ProductoAlmacen implements DAOProductoAlmacen {
    private Connection con;

    public ProductoAlmacen(Connection con) {
        this.con = con;
    }

    @Override
    public void crearTabla() {
        String sentenciaSQL = "CREATE TABLE IF NOT EXISTS ProductoAlmacen (" +
                "id INT NOT NULL, " +
                "nombre VARCHAR(50) NOT NULL, " +
                "calorias INT NOT NULL, " +
                "gluten BOOLEAN NOT NULL, " +
                "precio DOUBLE NOT NULL, " +
                "PRIMARY KEY (id)" +
                ");";

        try (Statement statement = con.createStatement()){
            statement.execute(sentenciaSQL);
        } catch (SQLException e) {
            System.out.println("Error al crear la tabla ProductoAlmacen.");
        }
    }

    @Override
    public void insertar(Producto producto) throws ClauException {
        String sentenciaSQL = "INSERT INTO ProductoAlmacen (id, nombre, calorias, gluten, precio) values (?, ?, ?, ?, ?);";

        try (PreparedStatement sentenciaPreparada = con.prepareStatement(sentenciaSQL)){
            sentenciaPreparada.setInt(1, producto.getId());
            sentenciaPreparada.setString(2, producto.getNombre());
            sentenciaPreparada.setInt(3, producto.getCalorias());
            sentenciaPreparada.setBoolean(4, producto.isGluten());
            sentenciaPreparada.setDouble(5, producto.getPrecio());
            sentenciaPreparada.executeUpdate();

        } catch (SQLException e) {
            System.out.println("Error al insertar los datos del producto.");
            Util.saveLogs(e);
            throw new ClauException(e);
        }

    }

    @Override
    public void eliminar(Integer idProducto) {
        String sentencia = "DELETE FROM ProductoAlmacen WHERE id = " + idProducto + ";";

        try (Statement statement = con.createStatement()) {
            statement.execute(sentencia);
            System.out.println("Se ha eliminado el producto con éxito.");
        } catch (SQLException e) {
            System.out.println("Error al eliminar el producto.");
        }
    }

    @Override
    public void actualitzar(Producto producto) {

        String sentencia = "UPDATE ProductoAlmacen SET nombre = ?, calorias = ?, gluten = ?, precio = ? WHERE id = ?;";
        try (PreparedStatement preparedStatement = con.prepareStatement(sentencia)) {
            preparedStatement.setInt(5, producto.getId());
            preparedStatement.setString(1, producto.getNombre());
            preparedStatement.setInt(2, producto.getCalorias());
            preparedStatement.setBoolean(3, producto.isGluten());
            preparedStatement.setDouble(4, producto.getPrecio());
            preparedStatement.executeUpdate();
            System.out.println("Se ha actualizado el producto.");
        } catch (SQLException e) {
            System.out.println("Error al actualizar el producto.");
        }
    }

    @Override
    public List<Producto> recuperar() {
        List<Producto> productos = new ArrayList<>();

        try (Statement statement = con.createStatement()) {
            String sentencia = "SELECT * FROM ProductoAlmacen;";
            ResultSet rs = statement.executeQuery(sentencia);
            while (rs.next()) {
                Producto producto = new Producto(rs.getInt("id"), rs.getString("nombre"), rs.getInt("calorias"), rs.getBoolean("gluten"), rs.getDouble("precio"));
                productos.add(producto);
            }
        } catch (SQLException e) {
            System.out.println("Error al recuperar los datos.");
        }

        return productos;
    }

    @Override
    public Producto recuperarPerId(Integer idProducto) {
        try (Statement statement = con.createStatement()) {
            String sentencia = "SELECT * FROM ProductoAlmacen WHERE id = " + idProducto + ";";
            ResultSet rs = statement.executeQuery(sentencia);
            if (rs.next()) {
                return new Producto(rs.getInt("id"), rs.getString("nombre"), rs.getInt("calorias"), rs.getBoolean("gluten"), rs.getDouble("precio"));
            }
        } catch (SQLException e) {
            System.out.println("Error al recuperar los datos.");
        }
        return null;
    }

    @Override
    public boolean buscarPorNombre(String nombreProducto) {
        try (Statement statement = con.createStatement()) {
            String sentencia = "SELECT * FROM ProductoAlmacen WHERE nombre = '" + nombreProducto + "';";
            ResultSet rs = statement.executeQuery(sentencia);
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error al comprobar si este nombre ya existe.");
        }

        return false;
    }

    @Override
    public int getNextNumProducto() {
        try (Statement statement = con.createStatement()) {
            String sentencia = "SELECT MAX(id) FROM ProductoAlmacen ;";
            ResultSet rs = statement.executeQuery(sentencia);
            if (rs.next()) {
                return rs.getInt(1) + 1;
            }
        } catch (SQLException e) {
            System.out.println("Error al recuperar el nuevo número de producto.");
        }

        return 1;
    }


    public List<Producto> buscarSinGluten() {
        List<Producto> productos = new ArrayList<>();
        try (Statement statement = con.createStatement()) {
            String sentencia = "SELECT * FROM ProductoAlmacen WHERE gluten = 0;";
            ResultSet rs = statement.executeQuery(sentencia);
            while (rs.next()) {
                Producto producto = new Producto(rs.getInt("id"), rs.getString("nombre"), rs.getInt("calorias"), rs.getBoolean("gluten"), rs.getDouble("precio"));
                productos.add(producto);
            }
        } catch (SQLException e) {
            System.out.println("Error al recuperar los datos.");
        }

        return productos;
    }

}
