package clases_jdbc;

import clases.Producto;
import dao.DAOProducto;
import dao.DAOProductoAlmacen;
import gestion_excepcion.ClauException;
import util.Util;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
/**
 * @author Bryan Oncoy
 */
public class ProductoJDBC implements DAOProducto {
    private Connection con;

    public ProductoJDBC(Connection con) {
        this.con = con;
    }

    @Override
    public void crearTabla() {
        String sentenciaSQL = "CREATE TABLE IF NOT EXISTS Producto (" +
                "id INT NOT NULL, " +
                "nombre VARCHAR(50) NOT NULL, " +
                "calorias INT NOT NULL, " +
                "gluten BOOLEAN NOT NULL, " +
                "precio DOUBLE NOT NULL, " +
                "numPedido INT NOT NULL," +
                "FOREIGN KEY (numPedido) REFERENCES Pedido(numPedido)" +
                ");";

        try (Statement statement = con.createStatement()) {
            statement.execute(sentenciaSQL);
        } catch (SQLException e) {
            System.out.println("Error al crear la tabla Producto.");
        }
    }

    @Override
    public void insertar(List<Producto> productos, Integer numPedido) throws ClauException {
        String sentenciaSQL = "INSERT INTO Producto (id, nombre, calorias, gluten, precio, numPedido) values (?, ?, ?, ?, ?, ?);";

        for (Producto producto : productos) {
            try (PreparedStatement sentenciaPreparada = con.prepareStatement(sentenciaSQL)) {
                sentenciaPreparada.setInt(1, producto.getId());
                sentenciaPreparada.setString(2, producto.getNombre());
                sentenciaPreparada.setInt(3, producto.getCalorias());
                sentenciaPreparada.setBoolean(4, producto.isGluten());
                sentenciaPreparada.setDouble(5, producto.getPrecio());
                sentenciaPreparada.setInt(6, numPedido);
                sentenciaPreparada.executeUpdate();

            } catch (SQLException e) {
                System.out.println("Error al insertar los datos del producto.");
                Util.saveLogs(e);
                throw new ClauException(e);
            }
        }
    }

    @Override
    public void eliminarPorNumPedido(Integer numPedido) {
        String sentencia = "DELETE FROM Producto WHERE numPedido = " + numPedido + ";";

        try (Statement statement = con.createStatement()) {
            statement.execute(sentencia);
            System.out.println("Se han eliminado los productos con éxito.");
        } catch (SQLException e) {
            System.out.println("Error al eliminar los productos.");
        }
    }

    @Override
    public List<Producto> recuperarPorNumPedido(Integer numPedido) {
        List<Producto> productos = new ArrayList<>();

        try (Statement statement = con.createStatement()) {
            String sentencia = "SELECT * FROM Producto WHERE numPedido = '" + numPedido + "';";
            ResultSet rs = statement.executeQuery(sentencia);
            while (rs.next()) {
                Producto producto = new Producto(rs.getInt("id"), rs.getString("nombre"), rs.getInt("calorias"), rs.getBoolean("gluten"), rs.getDouble("precio"));
                productos.add(producto);
            }
        } catch (SQLException e) {
            System.out.println("Error al recuperar los datos.");
        }

        return productos;
    }

}
