package clases_jdbc;

import clases.Cliente;
import clases.Pedido;
import dao.DAOCliente;
import gestion_excepcion.ClauException;
import util.Util;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bryan Oncoy
 */
public class ClienteJDBC implements DAOCliente {
    private Connection con;

    public ClienteJDBC(Connection con) {
        this.con = con;
    }

    @Override
    public void crearTabla() {
        String sentenciaSQL = "CREATE TABLE IF NOT EXISTS Cliente (" +
                "dni CHAR(9) NOT NULL, " +
                "nombre VARCHAR(50) NOT NULL, " +
                "telefono INT(9) NOT NULL, " +
                "calle VARCHAR(50) NOT NULL, " +
                "PRIMARY KEY (dni)" +
                ");";

        try (Statement statement = con.createStatement()) {
            statement.execute(sentenciaSQL);
        } catch (SQLException e) {
            System.out.println("Error al crear la tabla Cliente.");
        }
    }

    @Override
    public void insertar(Cliente cliente) throws ClauException {
        String sentenciaSQL = "INSERT INTO Cliente (dni, nombre, telefono, calle) values (?, ?, ?, ?);";
        try (PreparedStatement sentenciaPreparada = con.prepareStatement(sentenciaSQL)) {
            //Cogemos el ID de cada producto que contenga nuestro pedido

            sentenciaPreparada.setString(1, cliente.getDni());
            sentenciaPreparada.setString(2, cliente.getNombre());
            sentenciaPreparada.setInt(3, cliente.getTelefono());
            sentenciaPreparada.setString(4, cliente.getCalle());
            sentenciaPreparada.executeUpdate();

        } catch (SQLException e) {
            System.out.println("Error al insertar los datos del cliente.");
            Util.saveLogs(e);
            throw new ClauException(e);
        }

    }

    @Override
    public void eliminar(String dniCliente) {
        PedidoJDBC pedidoJDBC = new PedidoJDBC(con);
        pedidoJDBC.eliminarPorDni(dniCliente);

        String sentencia = "DELETE FROM Cliente WHERE dni = '" + dniCliente + "';";
        try (Statement statement = con.createStatement()) {
            statement.execute(sentencia);
            System.out.println("Se ha eliminado el cliente con éxito.");
        } catch (SQLException e) {
            System.out.println("Error al eliminar el cliente.");
        }
    }

    @Override
    public void actualitzar(Cliente cliente) {
        String sentencia = "UPDATE Cliente SET nombre = ?, telefono = ?, calle = ? WHERE dni = ?;";
        try (PreparedStatement preparedStatement = con.prepareStatement(sentencia)) {
            preparedStatement.setString(1, cliente.getNombre());
            preparedStatement.setInt(2, cliente.getTelefono());
            preparedStatement.setString(3, cliente.getCalle());
            preparedStatement.setString(4, cliente.getDni());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error al actualizar el pedido.");
        }
    }

    @Override
    public List<Cliente> recuperar() {
        List<Cliente> clientes = new ArrayList<>();
        try (Statement statement = con.createStatement()) {

            PedidoJDBC pedidoJDBC = new PedidoJDBC(con);
            String sentencia = "SELECT * FROM Cliente;";
            ResultSet rs = statement.executeQuery(sentencia);
            while (rs.next()) {
                String dni = rs.getString("dni");
                List<Pedido> pedidos = pedidoJDBC.recuperarPerDni(dni);
                Cliente cliente = new Cliente(
                        dni,
                        rs.getString("nombre"),
                        rs.getInt("telefono"),
                        rs.getString("calle"),
                        pedidos
                );

                clientes.add(cliente);
            }
        } catch (SQLException e) {
            System.out.println("Error al recuperar los datos del cliente.");
        }

        return clientes;
    }

    @Override
    public Cliente recuperarPerId(String dniCliente) {
        try (Statement statement = con.createStatement()) {
            PedidoJDBC pedidoJDBC = new PedidoJDBC(con);
            String sentencia = "SELECT * FROM Cliente WHERE dni = '" + dniCliente + "';";
            ResultSet rs = statement.executeQuery(sentencia);
            if (rs.next()) {
                String dni = rs.getString("dni");
                List<Pedido> pedidos = pedidoJDBC.recuperarPerDni(dni);
                return new Cliente(
                        dni,
                        rs.getString("nombre"),
                        rs.getInt("telefono"),
                        rs.getString("calle"),
                        pedidos
                );
            }
        } catch (SQLException e) {
            System.out.println("Error al recuperar los datos.");
        }

        return null;
    }
}
