package clases_jdbc;

import clases.Pedido;
import clases.Producto;
import dao.DAOPedido;
import gestion_excepcion.ClauException;
import util.Util;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
/**
 * @author Bryan Oncoy
 */
public class PedidoJDBC implements DAOPedido {
    private Connection con;

    public PedidoJDBC(Connection con) {
        this.con = con;
    }

    @Override
    public void crearTabla() {
        String sentenciaSQL = "CREATE TABLE IF NOT EXISTS Pedido (" +
                "numPedido INT NOT NULL, " +
                "precioTotal DOUBLE NOT NULL, " +
                "fechaPedido DATE NOT NULL, " +
                "fechaPago DATE, " +
                "kcalTotal INT NOT NULL, " +
                "pagado BOOLEAN NOT NULL, " +
                "dniCliente CHAR(9) NOT NULL, " +
                "PRIMARY KEY (numPedido), " +
                "FOREIGN KEY (dniCliente) REFERENCES Cliente(dni)" +
                ");";

        try (Statement statement = con.createStatement()) {
            statement.execute(sentenciaSQL);
        } catch (SQLException e) {
            System.out.println("Error al crear la tabla Pedido.");
        }
    }

    @Override
    public void insertar(Pedido pedido) throws ClauException {
        String sentenciaSQL = "INSERT INTO Pedido (numPedido, precioTotal, fechaPedido, fechaPago, kcalTotal, pagado, dniCliente) values (?, ?, ?, ?, ?, ?, ?);";

        try (PreparedStatement sentenciaPreparada = con.prepareStatement(sentenciaSQL)) {

            sentenciaPreparada.setInt(1, pedido.getNumPedido());
            sentenciaPreparada.setDouble(2, pedido.getPrecioTotal());
            sentenciaPreparada.setDate(3, Date.valueOf(pedido.getFechaPedido().toLocalDate()));
            if (pedido.getFechaPago() == null) {
                sentenciaPreparada.setDate(4, null);
            } else {
                sentenciaPreparada.setDate(4, Date.valueOf(pedido.getFechaPago().toLocalDate()));
            }
            sentenciaPreparada.setInt(5, pedido.getKcalTotal());
            sentenciaPreparada.setBoolean(6, pedido.isPagado());
            sentenciaPreparada.setString(7, pedido.getDniCliente());
            sentenciaPreparada.executeUpdate();

            ProductoJDBC productoJDBC = new ProductoJDBC(con);
            productoJDBC.insertar(pedido.getProductos(), pedido.getNumPedido());
        } catch (SQLException e) {
            System.out.println("Error al insertar los datos del pedido.");
            Util.saveLogs(e);
            throw new ClauException(e);
        }
    }

    @Override
    public void eliminar(Integer idPedido) {
        ProductoJDBC productoJDBC = new ProductoJDBC(con);
        productoJDBC.eliminarPorNumPedido(idPedido);

        String sentencia = "DELETE FROM Pedido WHERE id = " + idPedido + ";";
        try (Statement statement = con.createStatement()) {
            statement.execute(sentencia);
            System.out.println("Se ha eliminado el pedido con éxito.");
        } catch (SQLException e) {
            System.out.println("Error al eliminar el pedido.");
        }
    }

    public void eliminarPorDni(String dniCliente) {
        List<Pedido> pedidos = recuperarPerDni(dniCliente);
        ProductoJDBC productoJDBC = new ProductoJDBC(con);
        for (Pedido pedido : pedidos) {
            productoJDBC.eliminarPorNumPedido(pedido.getNumPedido());
        }

        String sentencia = "DELETE FROM Pedido WHERE dniCliente = '" + dniCliente + "';";
        try (Statement statement = con.createStatement()) {
            statement.execute(sentencia);
            System.out.println("Se ha eliminado el pedido con éxito.");
        } catch (SQLException e) {
            System.out.println("Error al eliminar el pedido.");
        }
    }

    @Override
    public void actualitzar(Pedido pedido) {
        String sentencia = "UPDATE Pedido SET fechaPago = ?, pagado = ? WHERE numPedido = ?;";
        try (PreparedStatement preparedStatement = con.prepareStatement(sentencia)) {

            preparedStatement.setInt(3, pedido.getNumPedido());
            preparedStatement.setDate(1, Date.valueOf(LocalDateTime.now().toLocalDate()));
            preparedStatement.setBoolean(2, true);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error al actualizar el pedido.");
        }
    }

    @Override
    public List<Pedido> recuperar() {
        List<Pedido> pedidos = new ArrayList<>();

        try (Statement statement = con.createStatement()) {
            String sentencia = "SELECT * FROM Pedido;";
            ResultSet rs = statement.executeQuery(sentencia);
            while (rs.next()) {
                pedidos.add(recuperarPedido(rs));
            }
        } catch (SQLException e) {
            System.out.println("Error al recuperar los datos.");
        }

        return pedidos;
    }

    @Override
    public Pedido recuperarPerId(Integer idPedido) {
        try (Statement statement = con.createStatement()) {
            String sentencia = "SELECT * FROM Pedido WHERE numPedido = " + idPedido + ";";
            ResultSet rs = statement.executeQuery(sentencia);
            if (rs.next()) {
                return recuperarPedido(rs);
            }
        } catch (SQLException e) {
            System.out.println("Error al recuperar los datos.");
        }
        return null;
    }

    @Override
    public int getNextNumPedido() {
        try (Statement statement = con.createStatement()) {
            String sentencia = "SELECT MAX(numPedido) FROM Pedido;";
            ResultSet rs = statement.executeQuery(sentencia);
            if (rs.next()) {
                return rs.getInt(1) + 1;
            }
        } catch (SQLException e) {
            System.out.println("Error al recuperar el nuevo número de pedido.");
        }

        return 1;
    }

    public List<Pedido> recuperarPerDni(String dniCliente) {
        List<Pedido> pedidos = new ArrayList<>();

        try (Statement statement = con.createStatement()) {
            String sentencia = "SELECT * FROM Pedido WHERE dniCliente = '" + dniCliente + "';";
            ResultSet rs = statement.executeQuery(sentencia);
            while (rs.next()) {
                pedidos.add(recuperarPedido(rs));
            }
        } catch (SQLException e) {
            System.out.println("Error al recuperar los datos del pedido.");
        }

        return pedidos;
    }

    @Override
    public List<Pedido> buscarSinPagar() {
        List<Pedido> pedidos = new ArrayList<>();
        try (Statement statement = con.createStatement()) {
            String sentencia = "SELECT * FROM Pedido WHERE pagado = 0;";
            ResultSet rs = statement.executeQuery(sentencia);
            while (rs.next()) {
                pedidos.add(recuperarPedido(rs));
            }
        } catch (SQLException e) {
            System.out.println("Error al recuperar los datos.");
        }

        return pedidos;
    }

    private Pedido recuperarPedido(ResultSet rs) throws SQLException {
        ProductoJDBC productoJDBC = new ProductoJDBC(con);

        List<Producto> productos = productoJDBC.recuperarPorNumPedido(rs.getInt("numPedido"));

        LocalDateTime fechaPago = null;
        Timestamp fechaP = rs.getTimestamp("fechaPago");
        if (fechaP != null) {
            fechaPago = fechaP.toLocalDateTime();
        }

        return new Pedido(
                rs.getInt("numPedido"),
                rs.getDouble("precioTotal"),
                rs.getTimestamp("fechaPedido").toLocalDateTime(),
                fechaPago,
                rs.getInt("kcalTotal"),
                productos,
                rs.getBoolean("pagado"),
                rs.getString("dniCliente")
        );
    }
}
