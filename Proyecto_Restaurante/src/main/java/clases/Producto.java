package clases;

import javafx.scene.control.Button;
/**
 * @author Liling Xu y Bryan Oncoy
 */
public class Producto {
    private int id;
    private String nombre;
    private int calorias;
    private boolean gluten;
    private double precio;
    private Button botonEditar;
    private Button botonEliminar;

    public Producto(){
    }

    public Producto(int id, String nombre, int calorias, boolean gluten, double precio) {
        this.id = id;
        this.nombre = nombre;
        this.calorias = calorias;
        this.gluten = gluten;
        this.precio = precio;
        this.botonEditar = new Button("Editar");
        this.botonEliminar = new Button("Eliminar");
    }

    @Override
    public String toString() {
        return nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getCalorias() {
        return calorias;
    }

    public void setCalorias(int calorias) {
        this.calorias = calorias;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isGluten() {
        return gluten;
    }

    public void setGluten(boolean gluten) {
        this.gluten = gluten;
    }

    public Button getBotonEditar() {
        return botonEditar;
    }

    public void setBotonEditar(Button botonEditar) {
        this.botonEditar = botonEditar;
    }

    public Button getBotonEliminar() {
        return botonEliminar;
    }

    public void setBotonEliminar(Button botonEliminar) {
        this.botonEliminar = botonEliminar;
    }
}
