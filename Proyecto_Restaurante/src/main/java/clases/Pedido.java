package clases;

import javafx.scene.control.Button;

import java.time.LocalDateTime;
import java.util.List;
/**
 * @author Liling Xu y Bryan Oncoy
 */
public class Pedido {
    private int numPedido;
    private double precioTotal;
    private LocalDateTime fechaPedido;
    private LocalDateTime fechaPago;
    private int kcalTotal;
    private List<Producto> productos;
    private boolean pagado;
    private String dniCliente;
    private Button botonPagar;

    public Pedido() {
    }

    //MongoDB - Crear pedido para guardar
    public Pedido(int numPedido, LocalDateTime fechaPedido, List<Producto> productos, boolean pagado) {
        this.numPedido = numPedido;
        precioTotal = 0;
        kcalTotal = 0;
        for (Producto producto : productos) {
            precioTotal += producto.getPrecio();
            kcalTotal += producto.getCalorias();
        }
        this.fechaPedido = fechaPedido;
        if (pagado)
            this.fechaPago = LocalDateTime.now();
        else
            this.fechaPago = null;
        this.productos = productos;
        this.pagado = pagado;
        this.botonPagar = new Button("Pagar");
    }

    //MongoDB - Crear pedido al recuperar
    public Pedido(int numPedido, double precioTotal, LocalDateTime fechaPedido, LocalDateTime fechaPago, int kcalTotal, List<Producto> productos, boolean pagado) {
        this.numPedido = numPedido;
        this.precioTotal = precioTotal;
        this.fechaPedido = fechaPedido;
        this.fechaPago = fechaPago;
        this.kcalTotal = kcalTotal;
        this.productos = productos;
        this.pagado = pagado;
        this.botonPagar = new Button("Pagar");
    }

    //JDBC - Crear pedido para guardar
    public Pedido(int numPedido, LocalDateTime fechaPedido, List<Producto> productos, boolean pagado, String dniCliente) {
        this.numPedido = numPedido;
        this.precioTotal = 0;
        this.kcalTotal = 0;
        for (Producto producto : productos) {
            this.precioTotal += producto.getPrecio();
            this.kcalTotal += producto.getCalorias();
        }
        this.fechaPedido = fechaPedido;
        if (pagado)
            this.fechaPago = LocalDateTime.now();
        else
            this.fechaPago = null;
        this.productos = productos;
        this.pagado = pagado;
        this.dniCliente = dniCliente;
        this.botonPagar = new Button("Pagar");
    }

    //JDBC - Crear pedido al recuperar
    public Pedido(int numPedido, double precioTotal, LocalDateTime fechaPedido, LocalDateTime fechaPago, int kcalTotal, List<Producto> productos, boolean pagado, String dniCliente) {
        this.numPedido = numPedido;
        this.precioTotal = precioTotal;
        this.fechaPedido = fechaPedido;
        this.fechaPago = fechaPago;
        this.kcalTotal = kcalTotal;
        this.productos = productos;
        this.pagado = pagado;
        this.dniCliente = dniCliente;
        this.botonPagar = new Button("Pagar");
    }

    public int getNumPedido() {
        return numPedido;
    }

    public void setNumPedido(int numPedido) {
        this.numPedido = numPedido;
    }

    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    public LocalDateTime getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(LocalDateTime fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    public LocalDateTime getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(LocalDateTime fechaPago) {
        this.fechaPago = fechaPago;
    }

    public int getKcalTotal() {
        return kcalTotal;
    }

    public void setKcalTotal(int kcalTotal) {
        this.kcalTotal = kcalTotal;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    public boolean isPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }

    public String getDniCliente() {
        return dniCliente;
    }

    public void setDniCliente(String dniCliente) {
        this.dniCliente = dniCliente;
    }

    public Button getBotonPagar() {
        return botonPagar;
    }

    public void setBotonPagar(Button botonPagar) {
        this.botonPagar = botonPagar;
    }
}
