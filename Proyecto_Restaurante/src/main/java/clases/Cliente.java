package clases;

import javafx.scene.control.Button;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Liling Xu y Bryan Oncoy
 */
public class Cliente {
    private String dni;
    private String nombre;
    private int telefono;
    private String calle;
    private List<Pedido> pedidos;
    private Button botonEliminar;
    private Button botonEditar;

    public Cliente() {
    }

    public Cliente(String dni, String nombre, int telefono, String calle) {
        this.dni = dni;
        this.nombre = nombre;
        this.telefono = telefono;
        this.calle = calle;
        this.pedidos = new ArrayList<>();
        this.botonEliminar = new Button("Eliminar");
        this.botonEditar = new Button("Editar");
    }

    public Cliente(String dni, String nombre, int telefono, String calle, List<Pedido> pedidos) {
        this.dni = dni;
        this.nombre = nombre;
        this.telefono = telefono;
        this.calle = calle;
        this.pedidos = pedidos;
        this.botonEliminar = new Button("Eliminar");
        this.botonEditar = new Button("Editar");
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    public Button getBotonEliminar() {
        return botonEliminar;
    }

    public void setBotonEliminar(Button botonEliminar) {
        this.botonEliminar = botonEliminar;
    }

    public Button getBotonEditar() {
        return botonEditar;
    }

    public void setBotonEditar(Button botonEditar) {
        this.botonEditar = botonEditar;
    }
}
