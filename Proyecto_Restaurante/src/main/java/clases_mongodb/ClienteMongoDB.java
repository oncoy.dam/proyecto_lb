package clases_mongodb;

import clases.Cliente;
import clases.Pedido;
import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;
import dao.DAOCliente;
import org.bson.Document;
import util.PropertyFile;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gt;
/**
 * @author Liling Xu
 */
public class ClienteMongoDB implements DAOCliente {
    private MongoClient con;

    public ClienteMongoDB(MongoClient con) {
        this.con = con;
    }

    @Override
    public void crearTabla() {

    }

    @Override
    public void insertar(Cliente cliente) {
        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Clientes");

        if (recuperarPerId(cliente.getDni()) == null) {
            collection.insertOne(guardarCliente(cliente));
            System.out.println("Cliente insertado");
        } else {
            System.out.println("Ya existe un cliente con DNI: " + cliente.getDni());
        }
    }

    @Override
    public void eliminar(String dniCliente) {
        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Clientes");

        DeleteResult deleteResult = collection.deleteMany(eq("dni", dniCliente));
        System.out.println("Se ha eliminado " + deleteResult.getDeletedCount() + " cliente -> " + dniCliente);

    }

    @Override
    public void actualitzar(Cliente cliente) {
        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Clientes");

        collection.updateOne(
                Filters.eq("dni", cliente.getDni()
                ),
                Updates.combine(
                        Updates.set("nombre", cliente.getNombre()),
                        Updates.set("telefono", cliente.getTelefono()),
                        Updates.set("calle", cliente.getCalle())
                )
        );

        System.out.println("Se ha actualizado el cliente con DNI: " + cliente.getDni());
    }

    @Override
    public List<Cliente> recuperar() {
        List<Cliente> clientes = new ArrayList<>();

        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Clientes");

        try (MongoCursor<Document> cursor = collection.find(gt("telefono", -10)).iterator()) {
            while (cursor.hasNext()) {
                Document d = cursor.next();
                clientes.add(leerCliente(d));
            }
        }

        return clientes;
    }


    @Override
    public Cliente recuperarPerId(String dniCliente) {
        Cliente cliente = null;

        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Clientes");

        try (MongoCursor<Document> cursor = collection.find(eq("dni", dniCliente)).iterator()) {
            while (cursor.hasNext()) {
                Document d = cursor.next();
                cliente = leerCliente(d);
            }
        }

        return cliente;
    }

    public static Document guardarCliente(Cliente cliente) {
        Document document = new Document();
        document.append("dni", cliente.getDni());
        document.append("nombre", cliente.getNombre());
        document.append("telefono", cliente.getTelefono());
        document.append("calle", cliente.getCalle());

        List<Document> list = new ArrayList<>();
        for (Pedido pedido : cliente.getPedidos()) {
            list.add(PedidoMongoDB.guardarPedido(pedido));
        }

        document.append("pedidos", list);
        return document;
    }

    public static Cliente leerCliente(Document d) {
        String dni = d.getString("dni");
        String nombre = d.getString("nombre");
        int telefono = d.getInteger("telefono");
        String calle = d.getString("calle");

        List<Pedido> pedidos = new ArrayList<>();

        List<Document> documentList = d.getList("pedidos", Document.class);
        for (Document doc : documentList) {
            pedidos.add(PedidoMongoDB.leerPedido(doc));
        }


        return new Cliente(dni, nombre, telefono, calle, pedidos);
    }
}