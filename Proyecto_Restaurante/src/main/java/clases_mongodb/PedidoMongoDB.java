package clases_mongodb;

import clases.Producto;
import clases.Pedido;
import com.mongodb.client.*;
import com.mongodb.client.model.*;
import dao.DAOPedido;

import org.bson.Document;
import org.bson.conversions.Bson;
import util.PropertyFile;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.*;
/**
 * @author Liling Xu
 */
public class PedidoMongoDB implements DAOPedido {
    private MongoClient con;

    public PedidoMongoDB(MongoClient con) {
        this.con = con;
    }

    @Override
    public void crearTabla() {

    }


    public void insertar(Pedido pedido) {
        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Clientes");
        Document doc = guardarPedido(pedido);
        collection.updateOne(
                Filters.eq("dni", pedido.getDniCliente()
                ),
                Updates.push("pedidos", doc)
        );
        System.out.println("Se ha aÃ±adido nuevo pedido al cliente: " + pedido.getDniCliente());
    }


    public void eliminar(Integer idPedido) {
        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Clientes");

        collection.updateOne(
                Filters.eq("pedidos.numPedido", idPedido
                ),
                Updates.unset("pedidos.$")
        );
        System.out.println("Se ha eliminado el pedido con numPedido: " + idPedido);
    }

    public void actualitzar(Pedido pedido) {
        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Clientes");

        collection.updateOne(
                Filters.eq("pedidos.numPedido", pedido.getNumPedido()
                ),
                Updates.combine(
                        Updates.set("pedidos.$.pagado", true),
                        Updates.set("pedidos.$.fechaPago", LocalDateTime.now())
                )
        );
        System.out.println("Se ha actualizado  el pedido con numPedido: " + pedido.getNumPedido());
    }

    public List<Pedido> recuperar() {
        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Clientes");

        MongoCursor<Document> cursor = collection.find(gt("telefono", -10)).iterator();

        return queryListPedidos(cursor);
    }


    public Pedido recuperarPerId(Integer idPedido) {
        Pedido pedido = null;

        List<Pedido> pedidos = recuperar();
        for (Pedido pedido1 : pedidos) {
            if (pedido1.getNumPedido() == idPedido) {
                pedido = pedido1;
                break;
            }
        }

        return pedido;
    }

    @Override
    public List<Pedido> recuperarPerDni(String dniCliente) {
        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Clientes");

        MongoCursor<Document> cursor = collection.find(eq("dni", dniCliente)).iterator();

        return queryListPedidos(cursor);
    }

    public List<Pedido> buscarSinPagar() {
        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Clientes");

        MongoCursor<Document> cursor = collection.find(eq("pagado", false)).iterator();

        return queryListPedidos(cursor);
    }

    public int getNextNumPedido() {
        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Clientes");

        Bson sort = Sorts.descending("pedidos.numPedido");
        List<Bson> pipeline = new ArrayList<>();
        pipeline.add(Aggregates.unwind("$pedidos"));
        pipeline.add(Aggregates.sort(sort));
        pipeline.add(Aggregates.limit(1));

        List<Document> documentList = collection.aggregate(pipeline).into(new ArrayList<>());

        if (documentList.isEmpty()) {
            return 1;
        } else {
            int numPedido = documentList.get(0).get("pedidos", Document.class).getInteger("numPedido");
            return numPedido+1;
        }
    }

    private List<Pedido> queryListPedidos(MongoCursor<Document> cursor) {
        List<Pedido> pedidos = new ArrayList<>();

        try (cursor) {
            while (cursor.hasNext()) {
                Document d = cursor.next();

                List<Document> documentList = d.getList("pedidos", Document.class);
                for (Document doc : documentList) {
                    pedidos.add(leerPedido(doc));
                }
            }
        }

        return pedidos;
    }

    public static Document guardarPedido(Pedido pedido) {
        Document document = new Document();
        document.append("numPedido", pedido.getNumPedido());
        document.append("precioTotal", pedido.getPrecioTotal());
        document.append("fechaPedido", pedido.getFechaPedido());
        if (pedido.getFechaPago() != null)
            document.append("fechaPago", pedido.getFechaPago());
        document.append("kcalTotal", pedido.getKcalTotal());

        List<Document> arrayList = new ArrayList<>();
        for (Producto producto : pedido.getProductos()) {
            arrayList.add(ProductoAlmacenMongoDB.guardarPedido(producto));
        }
        document.append("productos", arrayList);

        document.append("pagado", pedido.isPagado());
        return document;
    }

    public static Pedido leerPedido(Document d) {
        int numPedido = d.getInteger("numPedido");
        double precioTotal = d.getDouble("precioTotal");
        LocalDateTime fechaPedido = LocalDateTime.ofInstant(d.getDate("fechaPedido").toInstant(), ZoneId.systemDefault());

        LocalDateTime fechaPago = null;
        if (d.containsKey("fechaPago")) {
            fechaPago = LocalDateTime.ofInstant(d.getDate("fechaPago").toInstant(), ZoneId.systemDefault());
        }

        int kcalTotal = d.getInteger("kcalTotal");

        List<Producto> productos = new ArrayList<>();

        List<Document> documentList = d.getList("productos", Document.class);
        for (Document doc : documentList) {
            productos.add(ProductoAlmacenMongoDB.leerProducto(doc));
        }

        boolean pagado = d.getBoolean("pagado");
        return new Pedido(numPedido, precioTotal, fechaPedido, fechaPago, kcalTotal, productos, pagado);
    }
}