package clases_mongodb;

import clases.Producto;
import com.mongodb.client.*;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;
import dao.DAOProductoAlmacen;
import org.bson.Document;
import org.bson.conversions.Bson;
import util.PropertyFile;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.*;
/**
 * @author Liling Xu
 */
public class ProductoAlmacenMongoDB implements DAOProductoAlmacen {
    private MongoClient con;

    public ProductoAlmacenMongoDB(MongoClient con) {
        this.con = con;
    }

    @Override
    public void crearTabla() {

    }

    public void insertar(Producto producto) {
        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Productos");

        if (recuperarPerId(producto.getId()) == null) {
            collection.insertOne(guardarPedido(producto));
            System.out.println("Alimento insertado");
        } else {
            System.out.println("Ya existe un alimento con ID: " + producto.getId());
        }

    }


    public void eliminar(Integer idProducto) {
        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Productos");

        DeleteResult deleteResult = collection.deleteMany(eq("id", idProducto));
        System.out.println("Se ha eliminado " + deleteResult.getDeletedCount() + " alimento con ID: " + idProducto);

    }


    public void actualitzar(Producto producto) {
        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Productos");

        collection.updateOne(
                Filters.eq("id", producto.getId()
                ),
                Updates.combine(
                        Updates.set("nombre", producto.getNombre()),
                        Updates.set("calorias", producto.getCalorias()),
                        Updates.set("gluten", producto.isGluten()),
                        Updates.set("precio", producto.getPrecio())

                )
        );
        System.out.println("Se ha actualizado  el producto: " + producto.getNombre());


    }

    public List<Producto> recuperar() {
        List<Producto> productos = new ArrayList<>();

        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Productos");

        try (MongoCursor<Document> cursor = collection.find(gt("id", -10)).iterator()) {
            while (cursor.hasNext()) {
                Document d = cursor.next();
                productos.add(leerProducto(d));
            }
        }

        return productos;
    }

    public Producto recuperarPerId(Integer idAlimento) {
        Producto producto = null;

        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Productos");

        try (MongoCursor<Document> cursor = collection.find(eq("id", idAlimento)).iterator()) {
            while (cursor.hasNext()) {
                Document d = cursor.next();
                producto = leerProducto(d);
            }
        }

        return producto;
    }


    public boolean buscarPorNombre(String nombreProducto) {
        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Productos");

        try (MongoCursor<Document> cursor = collection.find(eq("nombre", nombreProducto)).iterator()) {
            if (cursor.hasNext()) {
                return true;
            }
        }

        return false;
    }

    public int getNextNumProducto() {
        MongoDatabase database = con.getDatabase(PropertyFile.getDatabase());
        MongoCollection<Document> collection = database.getCollection("Productos");

        Bson sort = Sorts.descending("id");
        List<Bson> pipeline = new ArrayList<>();
        pipeline.add(Aggregates.unwind("$id"));
        pipeline.add(Aggregates.sort(sort));
        pipeline.add(Aggregates.limit(1));

        List<Document> documentList = collection.aggregate(pipeline).into(new ArrayList<>());

        if (documentList.isEmpty()) {
            return 1;
        } else {
            int numPedido = documentList.get(0).getInteger("id");
            return numPedido+1;
        }
    }

    public static Document guardarPedido(Producto producto) {
        Document document = new Document();
        document.append("id", producto.getId());
        document.append("nombre", producto.getNombre());
        document.append("calorias", producto.getCalorias());
        document.append("gluten", producto.isGluten());
        document.append("precio", producto.getPrecio());
        return document;
    }

    public static Producto leerProducto(Document d) {
        int id = d.getInteger("id");
        String nombre = d.getString("nombre");
        int calorias = d.getInteger("calorias");
        boolean gluten = d.getBoolean("gluten");
        double precio = d.getDouble("precio");

        return new Producto(id, nombre, calorias, gluten, precio);
    }
}